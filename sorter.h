#ifndef SORTER_H
#define SORTER_H

#include "inclusion.h"

class Sorter {

 public:
    Sorter();
    Sorter(FILE &finput, u_int32_t numbers_to_read);
    ~Sorter();
    void read_file();
    void sort_numbers();
    void output_to_file(FILE *f_output);

    inline int get_number_of_elements() const { return number_of_elements_; }
    inline int is_EOF() const { if ( eof_reached_ == EOF) return true; return false; }

 private:
    FILE *foutput_, *finput_; // unique for each thread

    u_int32_t  *array_numbers_;
    u_int32_t number_of_elements_;
    static pthread_mutex_t file_accessor_mutex;
                                                // 10000000     10^7
    u_int32_t how_many_numbers_can_read_; //  10^7 should be calculated dependent from frequency of cores for better performance
                                            //  why such number was chosen http://i.imgur.com/P6beN3t.png , constants are  neglected
    int eof_reached_;


    Sorter(const Sorter &);
    void operator=(const Sorter &);
};

#endif // SORTER_H
