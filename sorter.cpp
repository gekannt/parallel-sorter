#include "sorter.h"

pthread_mutex_t Sorter::file_accessor_mutex = PTHREAD_MUTEX_INITIALIZER;
//int Sorter::current_number_of_used_destiptor = 0;
//int Sorter::eof_reached_ = 0;

Sorter::Sorter() {
  number_of_elements_ = 0;
  eof_reached_ =0;
}

Sorter::Sorter(FILE &finput,  u_int32_t numbers_to_read) {
  finput_ = &finput;

  eof_reached_ = 0;
  how_many_numbers_can_read_ = numbers_to_read;

  array_numbers_ = new (std::nothrow)u_int32_t[how_many_numbers_can_read_];

  if ( array_numbers_ == NULL) {
      std::cerr<< __FILE__ << __LINE__ << "not able to allocate enough memory";
      std::cerr <<" were asked bytes" << how_many_numbers_can_read_<<" there is no such amount of memory\n";
  }
 number_of_elements_ = 0;
}

Sorter::~Sorter() {
    if ( array_numbers_ != NULL) {
      delete []array_numbers_;
      array_numbers_ = NULL;
     }
}


void Sorter::read_file() {
   std::cout << "quantity of numbers can be read  " << how_many_numbers_can_read_  <<"\n";
  number_of_elements_ = 0;

  if ( eof_reached_  == EOF) { std::cout<<"eof reached, returning"<<std::endl;   return; }

   for(u_int32_t i=0; i< how_many_numbers_can_read_;i++) {
       eof_reached_ = fscanf(finput_,"%u", &array_numbers_[i]);
//       u_int32_t number = array_numbers_[i];
       if ( eof_reached_ == EOF) {
           std::cout<<"eof reached "<<std::endl;
           break;
         }
      number_of_elements_++;
   }
   std::cout<<"part of file is read"<<std::endl;
}

inline int compare  (const void *a, const void  *b) {
    return ( *(u_int32_t*) a- *(u_int32_t*)b );
}

void Sorter::sort_numbers() {
    if (number_of_elements_ < 2 ) return;
    qsort(array_numbers_,number_of_elements_,sizeof(u_int32_t),compare); // buffer arrays are not used
                                                                         // in-place sorting
    // using here merge sort  would require allocation of temporary arrays
    // because of limitations on memory it's better to use here  quick sort (average case performance is the same)
}

void Sorter::output_to_file(FILE *f_output) {

    for(u_int32_t i=0; i<number_of_elements_; i++) {
        fprintf(f_output,"%u\n",array_numbers_[i]);
    }
}
