#ifndef STORAGE_H
#define STORAGE_H

#include "inclusion.h"
#include "sorter.h"

class Server {

 public:
    Server(const char *input_file, const char *output_file);
    void divide();
    void merge_controller();


 private:
    int number_of_cores_; // number_of_files_to_store_;
    u_int64_t size_of_input_file_;
    const char *name_of_input_file_, *name_of_output_file;

    static std::vector <FILE *> files_desriptors_;
    static std::vector <std::string> file_names_;
    static pthread_mutex_t accessor_mutex;
    static u_int32_t memory_to_read_per_one_request_from_file_,
                     numbers_to_read_per_one_request_to_file_;

    void preparing_files();
    void merge_two_files(FILE *finput1, FILE *finput2, FILE *fout);



    static int get_number_of_cores();
    static u_int64_t get_size_of_file(const char *name_of_file);
    static FILE  *generate_file(std::vector <std::string> &storage_names);
    static void *action_division_threads(void *data);


//DISALLOW COPY CONSTRUCTOR AND ASSIGNMENT OPERATOR
    Server(const Server &);
    void operator=(const Server &);
};

#endif // STORAGE_H
