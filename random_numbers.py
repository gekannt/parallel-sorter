# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 16:29:36 2013

@author: sh
"""
import random

kNUMBER=2**30
kMAX_NUMBER=34666544
                            

kMIN_NUMBER=0
kNAME_FILE="numbers.txt"



def generate_numbers():
    file=open(kNAME_FILE,"w+")
    
    i=0
    while (i<kNUMBER):
     file.write(str(random.randint(kMIN_NUMBER,kMAX_NUMBER))+" ")
     i=i+1
        
    file.close()    

def main():
    generate_numbers()

if __name__ == "__main__":
    main()
