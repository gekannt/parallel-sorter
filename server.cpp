#include "server.h"

using namespace std;

pthread_mutex_t Server::accessor_mutex = PTHREAD_MUTEX_INITIALIZER;
//FILE **Server::holder_opened_temporary_files_;


//160 MB for one thread - if only one core -  !~~!!!!!!!!!!!!!!!! DON'T FORGET
// 80 MB for one thread - if two cores
// 40 MB                - if four
// and so on
                             // i.e. 160 MB - if thread is one

                                                           // 160000000 - good number
                    // here should be pointed TOTAL number of accessed memory !!!
/*  Здесь можно указывать по скольку данных может считывать программа из файлов
 *  Учитывая ограничения по заданию - 250 MB было выбрано 160 MB, остальные 90 MB про запас (фактически ещё 50 MB сверху свободно)
 *  Можно делать сделать ещё меньше - но при этом будет расти количество временных файлов
*/
u_int32_t Server::memory_to_read_per_one_request_from_file_ = 120000000; // размер указывется в байтах - взято 120 MB
/*                                                                  ^
 *                                                                  |
 *  Т.е. можно размер памяти занимаемой  уменьшить  через это число |, чем повышается эффективность по заданию - но потом теряем на слиянии
 *                                                                  |
 */
u_int32_t Server::numbers_to_read_per_one_request_to_file_ = memory_to_read_per_one_request_from_file_ / sizeof (u_int32_t);

vector <FILE* > Server::files_desriptors_;
vector <string> Server::file_names_;

Server::Server(const char *input_file, const char *output_file) {
    files_desriptors_.clear();
    size_of_input_file_  = this->get_size_of_file(input_file);
    number_of_cores_ = get_number_of_cores();
 // my number of cores is 2
//   number_of_cores_ = 8;

    cout<<number_of_cores_<<" cores are available and will be used\n";

    name_of_input_file_ = input_file;
    name_of_output_file = output_file;
}

void *Server::action_division_threads(void *data) {
   int return_from_here = 0;
   static int counter_of_reading_blocks_from_file = 0;

   while (!return_from_here) {
    Sorter *sorter = (Sorter * )data;
//    cout<<sorter->get_index()<<endl;

    pthread_mutex_lock(&accessor_mutex);

//    int current_number_of_descriptor = sorter->get_number_of_current_desriptor();
//    sorter->increment_descriptor_number();

    FILE *f = NULL;

    sorter->read_file();
    if ( sorter ->is_EOF() )
        return_from_here = 1;

        cout<<"counter  "<<counter_of_reading_blocks_from_file++<<"\n";
        f = generate_file(file_names_);
        files_desriptors_.push_back(f);

    pthread_mutex_unlock(&accessor_mutex);

//    FILE **fh;
//    generate_names(sfh);

//    if ( return_from_here != 1) {
       sorter->sort_numbers();
       cout<<"I have finished"<<sorter->get_number_of_elements()<<"  "<<endl;
       sorter->output_to_file(f);
//     }
   }
  pthread_exit((void *)0); // finished successfully
}


FILE *Server::generate_file(vector <string> &storage_names) {
    // creation of temporary that will be used for saving of sorted data
         char *name = tmpnam(NULL);
         printf("%s\n",name);
          FILE *f = fopen64(name,"w"); // it's not safe and not good to do (collision of names is possible)

//         holder[i] = tmpfile();
         if ( f == NULL) {
            cout<< " cannot create temporary file"<<endl;
            exit(0); // whole termination of program
         }

         storage_names.push_back(name);
//         file_names_.push_back(name);
    return f;
}

void Server::divide() {

   memory_to_read_per_one_request_from_file_ /= number_of_cores_;
   numbers_to_read_per_one_request_to_file_ /= number_of_cores_;

//   u_int32_t memory= memory_to_read_per_one_request_from_file_;
//   u_int32_t numbers= numbers_to_read_per_one_request_to_file_;

   cout<<"name of input file is "<<name_of_input_file_<<"\n";
    FILE *finput = fopen64 (name_of_input_file_,"r");


    if ( finput == NULL) {
        cerr<<"input file isn't found\n";
        exit(-1);
    }
    pthread_t threads[number_of_cores_];

//    mysorter.number_of_item_ = 0; //number_of_cores_;

    Sorter **just_sorter = new Sorter*[number_of_cores_];

    for ( int i=0; i< number_of_cores_; i++ ) {
        just_sorter[i] = new Sorter(*finput, numbers_to_read_per_one_request_to_file_);
        }

// creation of threads
    for (int i=0; i<number_of_cores_; i++ ) {
        if (  pthread_create(&threads[i], NULL, &Server::action_division_threads, just_sorter[i] )  != 0 ) {
             cerr<< "threads starting failed, don't trust results! return program\n";
             exit(-1);
          }
    }

  // waiting for created threads in main thread
    for( int i=0; i< number_of_cores_;i++) {
        if ( pthread_join(threads[i],NULL)  == 0 )
            cerr<<i <<" joined successfully\n";
        else {
          cerr<<i<< " joining failed! don't trust results, program should be reruned\n";
          exit(-1);
        }
    }

    puts("server has waited till end");
    cout<<endl;

    fclose(finput);
     for(int i =0; i< number_of_cores_; i++) {
         if ( just_sorter[i] != NULL)
          delete just_sorter[i];
     }
     if ( just_sorter != NULL) delete []just_sorter;

     //closing temporary files
//     int a= files_desriptors_.size();
     for(u_int32_t i =0; i< files_desriptors_.size(); i++) {
         fclose(files_desriptors_[i]);
     }
   files_desriptors_.clear();

}


void Server::preparing_files() {
    for (u_int32_t i=0; i<file_names_.size(); i++)
        cout<<file_names_[i]<<"\n";

    cout<<"number of elements "<<file_names_.size()<<"\n";


    vector <string> buf = file_names_;
    vector <string> buf2 = file_names_;

    //  delete empty files
    for(u_int32_t i=0; i< file_names_.size(); i++)  {
        if ( file_names_[i].length() == 0 || !get_size_of_file(file_names_[i].c_str()) )  {
            remove(file_names_[i].c_str() );
//            file_names_[i].erase(file_names_[i].begin(), file_names_[i].end());    // not so many files, so not a lot of looses in performance because of erasing in vector
//            file_names_.pop_back();
             buf2.pop_back();
           }
    }

    file_names_ = buf2;
    cout<<"opopopop\n";

    cout<<"number of elements "<<file_names_.size()<<"\n";

    for (u_int32_t i=0; i<file_names_.size(); i++)
        cout<<file_names_[i]<<"\n";
}

void Server::merge_two_files(FILE *finput1, FILE *finput2, FILE *fout) {
     std::cout<<"started merging\n";
        u_int32_t a,b,  left_number = 0;

        fscanf(finput1, "%u", &a);
        fscanf(finput2, "%u", &b);
        int EOF1_my = 0, EOF2_my = 0;

        while (true) {
            if ( a>b ) {
               fprintf( fout,"%u\n", b);
               if ( fscanf(finput2, "%u", &b)  == EOF) { EOF2_my = EOF; left_number = a; break; }
            } else  {
                fprintf( fout,"%u\n", a);
                if ( fscanf(finput1, "%u", &a) == EOF) { EOF1_my = EOF;  left_number = b; break; }
            }
        }

        //last number isn't written
        bool last_number_was_written = false;

      if ( EOF1_my == EOF) {
          // reading until we don't meet a number bigger than left number
               while ( fscanf(finput2, "%u", &a) != EOF) {
                   if ( left_number <= a) {
                      //fprintf(fout, "%u\n %u\n", left_number, a);
                       fprintf(fout, "%u\n",left_number);
                       fprintf(fout, "%u\n",a);
                       last_number_was_written = true;
                       break;
                   }
                   else {
                    fprintf(fout, "%u\n", a);
                   }
               }

               if (last_number_was_written == false)
                  fprintf(fout, "%u\n",left_number);
         // now just writing, doing of the forward loop shortens number of comparisons!!!
         // more code, but less expensive
                   while ( fscanf(finput2, "%u", &a) != EOF) {
                      fprintf(fout, "%u\n", a);                
                   }
//             fprintf(fout, "%u\n", a);
        } else if ( EOF2_my == EOF) {

          // reading until we don't meet a number bigger than left number
               while ( fscanf(finput1, "%u", &b) != EOF) {
                   if ( left_number <= b) {
//                      fprintf(fout, "%u\n %u\n", left_number, b);
                       fprintf(fout, "%u\n",left_number);
                       fprintf(fout, "%u\n",b);
                       last_number_was_written = true;
                      break;
                   }
                   else
                     fprintf(fout, "%u\n", b);
                 }

               if (last_number_was_written == false)
                 fprintf(fout, "%u\n",left_number);

         // just writing, doing of the forward loop shortens number of comparisons
                   while ( fscanf(finput1, "%u", &b) != EOF) {
                      fprintf(fout, "%u\n", b);
                   }
//                fprintf(fout, "%u\n", b);
       }

        std::cout<<"finished merging\n";
//        exit(0);
}



void Server::merge_controller() {
/*   слияние сделано в один поток, т.к. из-за отстутствия каких-либо трудоёмких операций (только сравнение происходит), нет смысла разбивать
 *   Сейчас, при слиянии, всё упирается в скорость операций ввода-вывода и производительности диска
 *   Диск, обычно намного медленнее по бьстродействию процессора
 *   и даже один поток, будет простаивать время, ожидая ввод-вывод
 *   Увеличение количества потоков, при использовании одного физического диска не даст никакого прироста, а будет лишь простой
 *
 *  При слиянии одним потоком, не наблюдается загрузки ядра полностью на 100%, что можно объяснить тем,
 *  что приходится ждать выполнения операций ввода-вывода от жесткого диска
 */

  this->preparing_files();

//    vector<string> buf2 = file_names_;
    int  index_files=0;


//    const char *d = NULL;
//    cout<<"size  "<<file_names_.size()<<"\n";
    for(u_int32_t i=0;i<file_names_.size();i++) {
//        d = file_names_[i].c_str();
//        int len= file_names_[i].length();
        cout<<file_names_[i]<<"\n";
    }




    for(int i=0; ; i++) {
          index_files =i*2;
//          const char *s1 = file_names_[index_files].c_str();
//          const char *s2 = file_names_[index_files+1].c_str();

          FILE *finp1 = fopen64(file_names_[index_files].c_str(),"r"),
               *finp2 = fopen64(file_names_[index_files+1].c_str(),"r");

          if ( finp1 == NULL || finp2 == NULL) {
              // cerr<<"crical error happened, temporary file has been lost\n";
              // exit(-1);
              cout<<"sorting has been finished successfully\n";
              return;
          }

//          vector<string> buf = file_names_;
          FILE *fout = NULL;
          if ( file_names_.size()-index_files == 2) { // pre-last merging together - only two
                                    // files are left. Giving the resulting file proper name.
             fout = fopen64(name_of_output_file,"w");
          }
          else {
             fout = generate_file(file_names_);
          }
//          buf = file_names_;

          merge_two_files(finp1,finp2,fout);
          fclose(finp1); fclose(finp2); fclose(fout);
          // cleaning used and unnecessary anymore files
          remove(file_names_[index_files].c_str());
          remove(file_names_[index_files+1].c_str());


    }
}


#ifndef WIN32  // Linux code
int Server::get_number_of_cores() {

    int number_of_cores= sysconf(_SC_NPROCESSORS_ONLN);
    if (number_of_cores == -1){
        cout<<"not possible to get number of cores, only one core will be used\n";
        return 1;
    }
    return number_of_cores;
}

#else  // Windows code

int Storage::get_number_of_cores() {
   SYSTEM_INFO sysinfo;
   GetSystemInfo(&sysinfo); // this function doesn't return value about finishing

   if ( GetLastError() == ERROR_SUCCESS) // operation completed successfully
       return sysinfo.dwNumberOfProcessors;

   cout<<"not possible to get number of cores, only one core will be used\n";
   return  1; //  at least one core should be
}
#endif


u_int64_t Server::get_size_of_file(const char *name_of_file) {
 struct stat64 stat_info;
// cout<<"name  "<<name_of_file<<"\n";
 if ( strlen(name_of_file) == 0 || strlen(name_of_file) == 1 )
    return 0;


 if ( stat64(name_of_file,&stat_info) == -1 ) {
     cout<<"can't get size of input file, terminating program\n";

     int a= strlen(name_of_file);
     exit(0);
  }
//  cout<<stat_info.st_size<<" bytes - size of your input file"<<"\n";
  return stat_info.st_size;

}
