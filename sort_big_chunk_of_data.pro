TEMPLATE = app
CONFIG += console
CONFIG -= qt

SOURCES += main.cpp \
    server.cpp \
    sorter.cpp

HEADERS += \
    inclusion.h \
    server.h \
    sorter.h

LIBS += -lpthread
