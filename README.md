## Задание ##

Имеется на входе файл неограниченного размера.
В файле содержатся целые беззнаковые числа.

Есть машина, на которой 256 мегабайт ОЗУ и бесконечный жёсткий диск.
Машина содержит от 4 и больше ядер.

Необходимо распараллелить.

Используя имеющиеся процессорные мощности по максимуму как только можно и
не вылезать за размеры ОЗУ. Т.е. задействовать все ядра.


На выходе нужно получить файл содержащий отсортированные числа.





многопоточная сортировка файлов  больших объёмов - тестировано на ~30 ГБ при использовании не более  256 мегабайт памяти

// ПОМЕТКИ
// В каталоге /tmp должно быть свободно места по крайней мере столько, сколько
весит входной файл + размер выделяемый
//
программе для чтения массивов
// Т.е. если файл входной 6 GB и программе выделено буфер 160 MB - необходимо 6GB
+ 160 MB свободного места
// там создаются временные файлы.
не получится.
Если места меньше, то временные файлы создать
// использование памяти можно регулировать - задавая какое количество памяти
вообще доступно
// для хранения считанных чисел - дальше она делится между количеством потоков
//это задаётся в server.h - в переменных
// u_int32_t Server::memory_to_read_per_one_request_from_file_; и
// u_int32_t Server::numbers_to_read_per_one_request_to_file_;
// Было выбрано 160 MB на числа (можно менять)
// количество ядер определяется через системные функции. При необходимости можно
вручную корректировать.
/*
____ КАК МОЖНО БЫЛО СДЕЛАТЬ ЛУЧШЕ _________
После написания пришло осознание, что, так как, числа все беззнаковые, то можно
было использовать
lexicographical comparisons, что улучшило бы скорость - не было бы конверсии из
строки в число и
назад числа в строку при выводе в файл.
Сейчас же, подаётся файл на вход, который содержит строки.
Сначала выполняется считывание - после конверсия в число
Происходит сравнение
Число конвертируется назад в строку
Затраты на конверсию можно было избежать так:
Считывать числа через fgets и fputs (они бы были как строки - только в
качестве разделителя тогда надо использовать \n)
Считать число с большей длиной большим числом
Если две строки имеют одинаковую длину, то проверить их через strcmp
*/
/*
*
Для проверка работоспособности необходимо было сгенирировать тестовые данные
числа генерировались через - random_numbers.py
Было проверено то, что для всех чисел a[i] <= a[i+1].
number_counter.cpp
Количество чисел во входном файле == количеству чисел в выходном (не было
потери данных)
-
*/
// изначально планировалось написать и под Linux и под Windows. В итоге работы
хватило и на Linux,
// остался код для определения кол-ва ядер и размера файла для обеих OS, дальшевсё разное
/*
Использовался GNU G++ compiler
gcc version 4.7.2 (Ubuntu/Linaro 4.7.2-2ubuntu1)
IDE
*/
разработки Qt Creator