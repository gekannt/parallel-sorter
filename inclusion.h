#ifndef INCLUSION_H
#define INCLUSION_H


/*  TODO
 * CHECK CODE DETECTING NUMBER OF CORES ON ANOTHER MACHINE WINDOWS
 * ПРОТЕСТИТЬ, КОГДА ВХОДНЫЕ ДАННЫЕ РАЗДЕЛЕНЫ НЕ  ТОЛЬКО ПРОБЕЛАМИ, НО И newline
 */

#include <iostream>
#include <stdint.h>
#include <cstdlib>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <vector>
#include <set>

// defining number of cores is platform dependent part
#ifndef WIN32  // for Linux
 #include <unistd.h>
 #include <sys/stat.h>
 #include <sys/wait.h>
 #include <pthread.h>
#else
 #include <windows.h>
#endif

//#define BACK_TO_MB 1024*1024


#define _LARGEFILE64_SOURCE
#define _FILE_OFFSET_BITS 64  // for reading files bigger than 4 GB

#endif // INCLUSION_H
